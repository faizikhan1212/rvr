module.exports = (question) => {
    if (/pet(s)?/i.test(question)) {
        return 'pets';
    }
    if (/smok(e|es|ed|ing)/i.test(question)) {
        return 'smoke';
    }
    if (/income/i.test(question)) {
        return 'income';
    }
    if (/bedrooms/i.test(question)) {
        return 'bedrooms';
    }
    if (/bath(room)?(s)/i.test(question)) {
        return 'bathrooms';
    }
    if (/square/i.test(question)) {
        return 'square';
    }
    if (/car(s)?/i.test(question)) {
        return 'cars';
    }
    return question;
};
