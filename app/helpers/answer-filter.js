module.exports = (answers) => {
    const filters = {};

    Object.keys(answers).forEach((key) => {
        const elem = JSON.parse(answers[key]);

        switch (elem.answerType) {
            case 'Yes/No': {
                filters[elem.questionType] = elem.answer.toLowerCase() === 'yes';
                break;
            }
            case 'Numeric': {
                filters[elem.questionType] = parseInt(elem.answer);
                break;
            }
            case 'Text': {
                filters[elem.questionType] = elem.answer;
                break;
            }
        }
    });

    return filters;
};