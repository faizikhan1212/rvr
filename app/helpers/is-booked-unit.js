module.exports = async (socket, bookableRepo, msg) => {
    if (msg.type === 'confirm-code') {
        return false;
    }

    const unitId = await bookableRepo.get(msg.phone, msg.unitId);

    if (unitId === 'unitId') {
        socket.emit('bot', {
            text: `You have already booked this unit. 😋 <a href="/#/company/${msg.company}">Want to see them?</a> 🙂`,
            phone: msg.phone,
            type: 'finish-book-a-tour',
            conToken: msg.conToken,
            company: msg.company,
            context: msg.context,
        });
        return true;
    }
    return false;
};