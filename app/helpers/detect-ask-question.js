module.exports = (question, dictionary) => {

    try {
        const dict = JSON.parse(dictionary);
        let amenities = null;
        let appliances = null;

        if (dict !== null) {
            amenities = dict.amenities !== null ?
                new RegExp(
                    dict.amenities.join('|')
                        .replace(' or ', '|')
                        .replace(' & ', '|'),
                    ['i'],
                ) : null;

            appliances = dict.appliances !== null ?
                new RegExp(
                    dict.appliances.join('|')
                        .replace(' or ', '|')
                        .replace(' & ', '|'),
                    ['i'],
                ) : null;
        }

        if (/\b((application|screen(ing)?) fee(s)?)\b/i.test(question)) {
            return {
                'category': 'applicationFee',
                'type': 'yes-no',
                'templateNo': `Don't worry you don't have to pay anything 🙂`,
                'templateYes': `Don't worry the fee is small, just $__template__. But keep in mind it's __template_second__.`,
            };
        }

        if (/\b(application.* process|qualification(s)?|security deposit.* pay.* later|reference(s)?|application.* fill out|support(ing)? document(s)?.* application|letter (of)? recommendation|pay stub(s)?|proof (of)? income|allow (kid(s)?|couple(s)?|infant(s)?|.*child(ren)?))\b/i.test(question)) {
            return {
                'category': 'applicationProcess',
                'type': 'string',
                'template': `Our qualification process is fairly simple so we encourage everyone to apply with their family. Once you tour the place and like it, you will complete an application form followed by providing proof of income and two references. We accept Pay stubs and Employer recommendation letters. `,
            };
        }

        if (/\b(renew lease|month-to-month|lease end(s)?)\b/i.test(question)) {
            return {
                'category': 'renewLease',
                'type': 'string',
                'template': `The current unit lease is for $__template__. After the lease ends, you can renew it or go month-to-month depending on the situation. Always happy to discuss this during the tour 🙂`,
            };
        }

        if (/\b(break(ing)?.* lease|lease termination|notice.+ move out|early.+ termination)\b/i.test(question)) {
            return {
                'category': 'leaseTermination',
                'type': 'string',
                'template': `We deal with early lease termination on a case by case basis as every lease is different. Please contact the office and explain your situation 🙂`,
            };
        }

        if (/\b(how much.* utilit(y|ies)|utility bill)\b/i.test(question)) {
            return {
                'category': 'utilityBill',
                'type': 'string',
                'template': `Utility bills vary based on usage so it’s hard to say accurately but it should be around __template__. You might want to google average rates in the area 🙂 For this unit you’ll have __template_second__`,
            };
        }

        if (/\butilit(y|ies)\b/i.test(question)) {
            return {
                'category': 'utilities',
                'type': 'enum',
                'template': `Sure, you’ll have __template__.`,
            };
        }

        if (/\b((bath(room)?(s)?|shower|toilet) shared)\b/i.test(question)) {
            return {
                'category': 'sharedWashroomBath',
                'type': 'yes-no',
                'templateNo': `It's not shared. You will have it all to yourself 🙂`,
                'templateYes': `Sharing is caring. You’ll share bathrooms with a few amazing people 🙂`,
            };
        }

        if (/\b(bath(s)?|bathroom(s)?|washroom(s)?|shower)\b/i.test(question)) {
            return {
                'category': 'bath',
                'type': 'number',
                'template': `You will have __template__ bathroom all to yourself.`,
            };
        }

        if (/\b(bed(room)?(s)?|room(s)?)\b/i.test(question)) {
            return {
                'category': 'bed',
                'type': 'number',
                'template': `It's a __template__ bedroom __template_second__`,
            };
        }

        if (/\b(big|sq ft|area !safe|square|dimension(s)?|floor plan)\b/i.test(question)) {
            return {
                'category': 'sqFt',
                'type': 'number',
                'template': `It's __template__ sq feet. Feel free to check out the photos. Pictures speak louder than words 🙂`,
            };
        }

        if (/\b((laundry|wash(er|ing)|dryer)(?!.*includ(.*)))\b/i.test(question)) {
            return {
                'category': 'sharedLaundry',
                'type': 'yes-no',
                'templateNo': `Sure. You’ll have in-unit laundry available 🙂`,
                'templateYes': `Sure. You’ll have shared laundry available 🙂`,
            };
        }

        if (/\b(except(ion)?.* pet(s)?|(support|assistance|service|guide).*(animal|dog)|not allowed.*(dog|cat)|letter.* pet)\b/i.test(question)) {
            return {
                'category': 'petPolicy',
                'type': 'yes-no',
                'templateNo': `Sorry but pets aren't allowed 😔 We can’t make any exceptions right now. Feel free to check our <a href="/#/company/__template__">other vacant units</a> that might have a different pet policy 🙂`,
                'templateYes': `Of course, we love pets. 🐶 Here’s our pet policy __template__`,
            };
        }

        if (/\b((dog(s)?|cat(s)?|breed(s)?).* (ok(ay)?|not listed))\b/i.test(question)) {
            return {
                'category': 'petPolicy',
                'type': 'string',
                'template': `We love pets but we do have strict guidelines 🙂 Here’s our pet policy again __template__. I’d suggest you to book a tour and then we can have a chat in person 🙂`,
            };
        }

        if (/\b(pet(s)?|dog(s)?|(?!lo)cat(s)?|breed(s)?)\b/i.test(question)) {
            return {
                'category': 'petFriendly',
                'type': 'yes-no',
                'templateNo': `Sorry but pet's aren't allowed 😔 Have a look at our <a href="/#/company/__template__">other vacant units</a>`,
                'templateYes': `Of course, we love pets. 🐶 Here’s our pet policy __template__`,
            };
        }

        if (/\b(section 8|cosigner|guarant(or|ee)|source(s)?.* income|benefit(s)?|voucher(s)?|trust fund(s)?|housing assistance|welfare|income.* low)\b/i.test(question)) {
            return {
                'category': 'sourceIncome',
                'type': 'string',
                'template': `Uhm, so we don’t discriminate based on source of income. Please go ahead and book a tour. We select renters based on many factors 🙂🙏`,
            };
        }

        if (/\b((smok|vap)(e(r)?|ing)|cigarette)\b/i.test(question)) {
            return {
                'category': 'noSmoking',
                'type': 'yes-no',
                'templateNo': `Sorry but any kind of smoking is strictly prohibited 😔`,
                'templateYes': `Don't worry, smoking is allowed 😉`,
            };
        }

        if (/\b(can(.t| not) pay|decorating|guest(s)? park(ing)?|do(n.t| not have).* job|pay.*install(ment(s)?)?|notice|pay.* upfront)\b/i.test(question)) {
            return {
                'category': 'decorating',
                'type': 'string',
                'template': `Please contact the office for any special needs and specific questions. I can answer a lot of questions but I’ll leave this to the humans 🙂`,
            };
        }

        if (/\b(deposit)\b/i.test(question)) {
            return {
                'category': 'deposit',
                'type': 'number',
                'template': `The security deposit is $__template__ (refundable) 🙂`,
            };
        }

        if (/\b(maintenance|renovation(s)?|inspection(s)?|improvement(s)?|change (the)? lock|key)\b/i.test(question)) {
            return {
                'category': 'maintenance',
                'type': 'string',
                'template': `We have dedicated maintenance staff to take care of any current or future requests. Once you sign a lease, we will go through the procedure of submitting maintenance requests. For questions about renovations and improvements, let’s discuss that during our tour 🙂`,
            };
        }

        if (/\b(parking|garage|car(s)?)\b/i.test(question)) {
            return {
                'category': 'parking',
                'type': 'yes-no',
                'templateNo': `Sorry this apartment has no parking lot 😔`,
                'templateYes': `There'll be a parking space for your car. Monthly fee is $__template__. It's a __template_second__ __template_third__ parking lot 😉`,
            };
        }

        if (/\b(guest)\b/i.test(question)) {
            return {
                'category': 'guest',
                'type': 'string',
                'template': `Our guest policy varies from property to property. Please get in touch with us over email or phone to discuss your specific case 🙂`,
            };
        }

        if (/\b(sublet(ting)?|sublease|airbnb)\b/i.test(question)) {
            return {
                'category': 'sublet',
                'type': 'string',
                'template': `Our sublease policy varies from property to property. Please get in touch with us over email or phone to discuss your specific case 🙂`,
            };
        }

        if (/\b((location|crime|theft|break((-)?ins)?|assault(s)?|robber(y|ies))|public transport(ation)?|restaurant|bar|park|area safe|grocery store|coffeehouse|dry cleaner|health club|bank|gas station|librar(y|ies))\b/i.test(question)) {
            return {
                'category': 'neighbourhood',
                'type': 'string',
                'template': `The location is amazing 🏔 Try to google nearby places to get a feel for the place 🙂 For questions about neighbourhood safety, I would encourage you to check with the local authorities 👮‍`,
            };
        }

        if (/\b(how much.+ (move in|total|due)|rent due|payment methods|accept (check(s)?|cheque(s)?)|online rent payments|move in rent|(check|cheque) pay)\b/i.test(question)) {
            return {
                'category': 'moveInDate',
                'type': 'string',
                'template': `The rent is $__template__. The security deposit is $__template_second__ (refundable). Due before __template_third__. We have different types of payment options that we can discuss during the tour 🙂`,
            };
        }

        if (/\b(move in|still (available|.* vacant)|hold|empty|people.* appl(y|ied)?|much interest|wait(ing)? list)\b/i.test(question)) {
            return {
                'category': 'moveIn',
                'type': 'string',
                'template': `The unit is available from __template__. We cannot hold it for anyone at the moment. I'd encourage you to book a tour now before it's rented 🙂`,
            };
        }

        if (/\b(unit shared|many people|shared unit|shared apartment|people.* liv(e|ing)|shared with other(s)?)\b/i.test(question)) {
            return {
                'category': 'unitShared',
                'type': 'yes-no',
                'templateNo': `It's not shared. You will have it all to yourself 🙂`,
                'templateYes': `Sharing is caring. You'll share this unit with __template__ amazing people 🙂`,
            };
        }

        if (/\b(lease)\b/i.test(question)) {
            return {
                'category': 'leaseTerm',
                'type': 'string',
                'template': `This unit lease is for __template__`,
            };
        }

        if (/\b(amenit(y|ies))\b/i.test(question)) {
            return {
                'category': 'amenities',
                'type': 'enum',
                'template': `We have some amazing amenities such as __template__`,
            };
        }

        if (/\b(appliance(s)?)\b/i.test(question)) {
            return {
                'category': 'appliances',
                'type': 'enum',
                'template': `Sure, you’ll have __template__`,
            };
        }

        if (amenities !== null && amenities.test(question)) {
            const query = amenities.exec(question)[0];

            return {
                'category': 'amenitiesCustom',
                'type': 'enum',
                'query': query,
                'template': `Yes, ${query} is included for this unit. We have some amazing amenities such as __template__`,
            };
        }

        if (appliances !== null && appliances.test(question)) {
            const query = appliances.exec(question)[0];

            return {
                'category': 'appliancesCustom',
                'type': 'enum',
                'query': query,
                'template': `Sure, you’ll have __template__`,
            };
        }

        if (/\b(rent)\b/i.test(question)) {
            return {
                'category': 'rent',
                'type': 'number',
                'template': `It's $__template__.`,
            };
        }

        return false;
    } catch (e) {
        console.log(e);
    }

};