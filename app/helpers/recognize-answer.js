const yesNo = require('../nlp/yes-no');
const nlp = require('compromise');

module.exports = async (answer, type) => {
    switch (type) {
        case 'Yes/No': {
            const yesNoIntent = (await yesNo.process('en', answer)).intent;
            return yesNoIntent.toLowerCase() !== 'none' ? yesNoIntent : 'no';
        }
        case 'Numeric': {
            return nlp(answer).values().numbers().reduce((acc, val) => acc + val);
        }
    }
    return answer;
};