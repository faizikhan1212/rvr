'use strict';

const questionRepo = require('../repos/question-repo');
const answerRepo = require('../repos/answer-repo');
const yesNoService = require('../services/yes-no-service');
const services = require('../services/services');
const detectQuestionType = require('../helpers/detect-question-type');
const unitFilter = require('../kafka/produces/unit-filter');

async function findAvailableUnits(socket, event, msg) {

    if (event.intent === 'find-available-units' || msg.type === 'not-created-questions-yet' || msg.type === 'find-available-units' && (parseInt(msg.position) === 0 || msg.position === null)) {
        const item = JSON.parse(await questionRepo.get(msg.company, 0));
        await answerRepo.destroy(msg.conToken);

        if (item !== null) {
            socket.emit('bot', {
                text: item.question,
                questionType: item.type,
                phone: msg.phone,
                type: 'find-available-units',
                conToken: msg.conToken,
                company: msg.company,
                position: 1,
            });

            if (item.type === 'Yes/No') {
                return yesNoService(socket, msg.phone, msg.conToken, msg.company);
            }
            return false;
        }

        return socket.emit('bot', {
            text: 'Sorry, the company has not created questions yet. 🤔',
            phone: msg.phone,
            type: 'not-created-questions-yet',
            conToken: msg.conToken,
            company: msg.company,
        });
    }

    if (msg.type === 'find-available-units') {

        const position = parseInt(msg.position);
        const prevPosition = position - 1;
        const prev = JSON.parse(await questionRepo.get(msg.company, prevPosition));
        const item = JSON.parse(await questionRepo.get(msg.company, position));
        const questionType = detectQuestionType(prev.question);

        await answerRepo.store(msg.conToken, prevPosition, msg.text, questionType, prev.type);

        if (item !== null) {

            socket.emit('bot', {
                text: item.question,
                questionType: item.type,
                phone: msg.phone,
                type: 'find-available-units',
                conToken: msg.conToken,
                company: msg.company,
                position: position + 1,
            });

            if (item.type === 'Yes/No') {
                return yesNoService(socket, msg.phone, msg.conToken, msg.company);
            }

            return false;
        }

        const answers = await answerRepo.list(msg.conToken);

        if (answers !== null) {
            unitFilter(answers, msg);
        }

        socket.emit('bot', {
            text: 'Thanks for answering our questions. Right behind me there\'s a list of units carefully chosen based on your needs. Pick up the one you like the most and I will help you to book it. Don\'t get upset if there are no suitable units. Try again later to find one!. Don\'t get upset if there are no suitable units. Try again later to find one! 😉',
            phone: msg.phone,
            type: 'find-available-units-end',
            conToken: msg.conToken,
            company: msg.company,
            position: 0,
        });
    }
    return services(socket, msg.phone, msg.conToken, msg.company);
}

module.exports = findAvailableUnits;