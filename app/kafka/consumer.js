const { KafkaConsumer } = require('node-rdkafka');
const { hostname } = require('os');
const questionRepo = require('../repos/question-repo');
const dictionaryRepo = require('../repos/dictionary-repo');
const availabilityRepo = require('../repos/availability-repo');
const bookAnotherTourServices = require('../services/book-another-tour-services');

module.exports = (io) => {

    require('dotenv').config();

    const host = hostname();
    const brokerList = process.env.KAFKA_BROKER_LIST;
    const topicPrefix = process.env.KAFKA_TOPIC_PREFIX;

    const globalConfig = {
        'debug': 'all',
        'client.id': ['lethub-chatbot', host].join('-'),
        'metadata.broker.list': brokerList,
        'group.id': host,
        'socket.keepalive.enable': true,
        'enable.auto.commit': false,
    };
    const topicConfig = {
        'auto.offset.reset': 'beginning',
    };

    const topics = [
        `${topicPrefix}-questions`,
        `${topicPrefix}-calendar`,
        `${topicPrefix}-lead-disqualified`,
        `${topicPrefix}-answer-question`,
        `${topicPrefix}-dictionary`,
        `${topicPrefix}-availability`,
    ];
    const consumer = new KafkaConsumer(globalConfig, topicConfig);

    consumer.on('event.log', (log) => {

    });

    consumer.on('event.error', (err)  => {
        console.error('Error from consumer');
        console.error(err);
    });

    consumer.on('ready', (arg) => {
        console.log('consumer ready.' + JSON.stringify(arg));
        consumer.subscribe(topics);
        consumer.consume();
    });

    consumer.on('data', async (message) => {
        const data = JSON.parse(message.value.toString());
        consumer.commit(message);

        switch (message.topic) {
            case `${topicPrefix}-questions`: {
                await questionRepo.store(data.company, data.questions);
                return false;
            }
            case `${topicPrefix}-dictionary`: {
                await dictionaryRepo.store(data.company, data.dictionary);
                return false;
            }
            case `${topicPrefix}-availability`: {
                await availabilityRepo.store(data.company, data.availability);
                return false;
            }
            case `${topicPrefix}-lead-disqualified`: {
                const socketId = data.socketId;
                const msg = data.msg;

                io.to(socketId).emit('bot', {
                    text: `I'm sorry this unit doesn't meet your needs. 😕 But I've got some other amazing suggestions for you. <a href="/#/company/${msg.company}">Want to see them? 🙂</a>`,
                    phone: msg.phone,
                    type: 'lead-disqualified',
                    conToken: msg.conToken,
                    company: msg.company,
                    context: 'book-a-tour',
                });

                return false;
            }
            case `${topicPrefix}-calendar`: {
                const socketId = data.socketId;
                const msg = data.msg;
                const availability = await availabilityRepo.get(msg.company);

                io.to(socketId).emit('bot', {
                    text: 'Awesome! Let\'s find a time that\'s available 🙂',
                    phone: msg.phone,
                    conToken: msg.conToken,
                    company: msg.company,
                    context: 'book-a-tour',
                });

                io.to(socketId).emit('bot', {
                    text: '',
                    type: 'calendar',
                    calendar: data.calendar,
                    availability: availability,
                    phone: msg.phone,
                    conToken: msg.conToken,
                    company: msg.company,
                    context: 'book-a-tour',
                });
                return false;
            }
            case `${topicPrefix}-answer-question`: {
                const socketId = data.socketId;
                const msg = data.msg;

                io.to(socketId).emit('bot', {
                    text: msg.text,
                    phone: msg.phone,
                    type: 'ask-a-question',
                    conToken: msg.conToken,
                    company: msg.company,
                    context: 'ask-a-question',
                });

                io.to(socketId).emit('bot', {
                    text: 'What\'s your next step? 😉',
                    phone: msg.phone,
                    type: 'ask-a-question',
                    conToken: msg.conToken,
                    company: msg.company,
                    context: 'ask-a-question',
                });

                return bookAnotherTourServices(io, socketId, msg.phone, msg.conToken, msg.company);
            }
        }
    });

    consumer.connect();
};


