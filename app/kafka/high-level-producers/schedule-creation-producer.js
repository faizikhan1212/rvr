const { HighLevelProducer } = require('node-rdkafka');
const { hostname } = require('os');

require('dotenv').config();

const host = hostname();
const brokerList = process.env.KAFKA_BROKER_LIST;
const topicPrefix = process.env.KAFKA_TOPIC_PREFIX;

const globalConfig = {
    'debug': 'all',
    'client.id': ['schedule-creation', host].join('-'),
    'metadata.broker.list': brokerList,
    'compression.codec': 'gzip',
    'retry.backoff.ms': 200,
    'message.send.max.retries': 10,
    'socket.keepalive.enable': true,
};
const topicConfig = {};

const highLevelProducer = new HighLevelProducer(globalConfig, topicConfig);

highLevelProducer.topic = `${topicPrefix}-schedule-creation`;

highLevelProducer.on('error', (error) => {
    console.error(error);
});

highLevelProducer.on('ready', (arg) => {
    console.log('producer ready.' + JSON.stringify(arg));
});

highLevelProducer.on('event.log', (log) => {

});

highLevelProducer.on('disconnected', (arg) => {
    console.log('producer disconnected. ' + JSON.stringify(arg));
});

highLevelProducer.connect();

module.exports = highLevelProducer;