const producer = require('../../kafka/producers/ask-question-producer');
const detectAskQuestion = require('../../helpers/detect-ask-question');
const dictionaryRepo = require('../../repos/dictionary-repo');

module.exports = async (msg, socketId) => {

    const dictionary = await dictionaryRepo.get(msg.company);
    const context = detectAskQuestion(msg.text, dictionary);

    if (context !== false) {
        const payload = Buffer.from(JSON.stringify({
            msg: msg,
            socketId: socketId,
            context: context
        }));

        return producer.produce(producer.topic, -1, payload);
    }

    return false;
};