const util = require('util');
const highLevelProducer = require('../../kafka/high-level-producers/schedule-creation-producer');
const produce = util.promisify(highLevelProducer.produce).bind(highLevelProducer);
const usernameRepo = require('../../repos/username-repo');


module.exports = async (msg) => {
    msg.username = await usernameRepo.getByPhone(msg);

    if (msg.username !== null) {

        const payload = Buffer.from(JSON.stringify({
            msg: msg,
        }));

        return await produce(highLevelProducer.topic, -1, payload, null, Date.now());
    }
};