const producer = require('../../kafka/producers/unit-filter-producer');
const answerFilter = require('../../helpers/answer-filter');

module.exports = (answers, msg) => {
    const filters = answerFilter(answers);

    const payload = Buffer.from(JSON.stringify({
        filters: filters,
        company: msg.company,
        conToken: msg.conToken,
    }));

    producer.produce(producer.topic, -1, payload);
};