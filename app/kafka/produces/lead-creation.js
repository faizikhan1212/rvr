const producer = require('../../kafka/producers/lead-creation-producer');
const answerFilter = require('../../helpers/answer-filter');
const usernameRepo = require('../../repos/username-repo');

module.exports = async (msg, status, socketId, answers) => {
    const filters = answers !== null ? answerFilter(answers) : {};
    const leadName = await usernameRepo.getByPhone(msg);

    if (leadName !== null) {
        const payload = Buffer.from(JSON.stringify({
            unitId: parseInt(msg.unitId),
            company: msg.company,
            msg: msg,
            name: leadName,
            status: status,
            phone: msg.phone,
            email: '',
            reason: '',
            socketId: socketId,
            filters: filters,
        }));
        try{
        producer.produce(producer.topic, -1, payload);
        }catch(e){console.log(e)}
    }
};