const { Producer } = require('node-rdkafka');
const { hostname } = require('os');

require('dotenv').config();

const host = hostname();
const brokerList = process.env.KAFKA_BROKER_LIST;
const topicPrefix = process.env.KAFKA_TOPIC_PREFIX;

const globalConfig = {
    'debug': 'all',
    'client.id': ['unit-filter', host].join('-'),
    'metadata.broker.list': brokerList,
    'compression.codec': 'gzip',
    'retry.backoff.ms': 200,
    'message.send.max.retries': 10,
    'socket.keepalive.enable': true,
};
const topicConfig = {};

const producer = new Producer(globalConfig, topicConfig);

producer.topic = `${topicPrefix}-unit-filter`;

producer.on('error', (error) => {
    console.error(error);
});

producer.on('ready', (arg) => {
    console.log('producer ready.' + JSON.stringify(arg));
});

producer.on('event.log', (log) => {

});

producer.on('disconnected', (arg) => {
    console.log('producer disconnected. ' + JSON.stringify(arg));
});

producer.connect();

module.exports = producer;