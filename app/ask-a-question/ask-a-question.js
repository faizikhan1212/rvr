'use strict';

const askQuestion = require('../kafka/produces/ask-question');
const bookAnotherTourServices = require('../services/book-another-tour-services');

async function askAQuestion(io, socket, event, msg) {

    if (msg.text === '') {
        return false;
    }

    if (msg.type === 'ask-a-question' && event.intent !== 'ask-a-question') {
        const isAsked = await askQuestion(msg, socket.id);

        if (isAsked === false) {
            socket.emit('bot', {
                text: 'I\'m a bit confused. Are you sure this is a question about this unit? 😄 Let\'s move on to the next one! 😉',
                phone: msg.phone,
                type: 'ask-a-question',
                conToken: msg.conToken,
                company: msg.company,
                context: msg.context,
            });

            return bookAnotherTourServices(io, socket.id, msg.phone, msg.conToken, msg.company);
        }

        return false;
    }

    if (msg.text !== 'Ask Another Question') {
        return socket.emit('bot', {
            text: 'Sure, feel free to ask me anything!',
            phone: msg.phone,
            type: 'ask-a-question',
            conToken: msg.conToken,
            company: msg.company,
            context: msg.context,
        });
    }

    return socket.emit('bot', {
        text: 'Of course, ask away!',
        phone: msg.phone,
        type: 'ask-a-question',
        conToken: msg.conToken,
        company: msg.company,
        context: msg.context,
    });
}

module.exports = askAQuestion;