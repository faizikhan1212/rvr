const { NlpManager } = require('node-nlp');

const manager = new NlpManager({ languages: ['en'] });

(async () => {

    /* REGISTER PHONE NUMBER */
    manager.addDocument('en', 'My phone is %phonenumber%', 'phone-number');
    manager.addDocument('en', 'My mobile phone is %phonenumber%', 'phone-number');
    manager.addDocument('en', 'My number is %phonenumber%', 'phone-number');

    /* REGISTER CODE CONFIRMATION */
    manager.addDocument('en', 'My code is %number%', 'confirm-code');
    manager.addDocument('en', 'Code is %number%', 'confirm-code');
    manager.addDocument('en', '%number%', 'confirm-code');

    /* SERVICES */
    manager.addDocument('en', 'Find Available Units', 'find-available-units');
    manager.addDocument('en', 'Book a Tour', 'book-a-tour');
    manager.addDocument('en', 'Ask a Question', 'ask-a-question');
    manager.addDocument('en', 'Ask Another Question', 'ask-a-question');

    /* WITHOUT CATEGORY */
    manager.addDocument('en', 'What is a pet policy?', 'without-category');
    manager.addDocument('en', 'Would a small dog be ok?', 'without-category');
    manager.addDocument('en', 'Would a small cat be okay?', 'without-category');
    manager.addDocument('en', 'My pet breed is not listed here. Can you still allow a pit bull?', 'without-category');

    await manager.train();

})();


module.exports = manager;