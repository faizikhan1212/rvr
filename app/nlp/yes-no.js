const { NlpManager } = require('node-nlp');

const manager = new NlpManager({ languages: ['en'] });

(async () => {

    /* YES */
    manager.addDocument('en', 'Yes', 'yes');
    manager.addDocument('en', 'yep', 'yes');
    manager.addDocument('en', 'yp', 'yes');
    manager.addDocument('en', 'Of course', 'yes');

    /* NO */
    manager.addDocument('en', 'No', 'no');
    manager.addDocument('en', 'Nop', 'no');
    manager.addDocument('en', 'Not', 'no');
    manager.addDocument('en', 'Nothing', 'no');

    await manager.train();

})();


module.exports = manager;