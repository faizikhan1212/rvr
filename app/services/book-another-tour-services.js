module.exports = (io, socketId, phone, conToken, company) => {
    return io.to(socketId).emit('bot', {
        text: '',
        services: [
            {
                value: 'Book a Tour',
                checked: false,
            },
            {
                value: 'Ask Another Question',
                checked: false,
            }
        ],
        phone: phone,
        conToken: conToken,
        company: company,
    });
};