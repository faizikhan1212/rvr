module.exports = (socket, phone, conToken, company) => {
    return socket.emit('bot', {
        text: '',
        services: [
            {
                value: 'Change Phone',
                checked: false,
            },
        ],
        phone: phone,
        conToken: conToken,
        company: company,
    });
};