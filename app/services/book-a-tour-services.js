module.exports = (socket, phone, conToken, company) => {
    return socket.emit('bot', {
        text: '',
        services: [
            {
                value: 'Book a Tour',
                checked: false,
            },
            {
                value: 'Ask a Question',
                checked: false,
            }
        ],
        phone: phone,
        conToken: conToken,
        company: company,
    });
};