module.exports = (socket, phone, conToken, company) => {
    return socket.emit('bot', {
        text: '',
        services: [
        ],
        phone: phone,
        type: 'services',
        conToken: conToken,
        company: company,
        position: 0,
    });
};