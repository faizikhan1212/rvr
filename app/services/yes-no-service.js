module.exports = (socket, phone, conToken, company) => {
    return socket.emit('bot', {
        text: '',
        services: [
            {
                value: 'Yes',
                checked: false,
            },
            {
                value: 'No',
                checked: false,
            },
        ],
        phone: phone,
        conToken: conToken,
        company: company,
    });
};