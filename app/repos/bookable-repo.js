'use strict';

const redis = require('redis');
const util = require('util');
const client = redis.createClient(process.env.REDIS_PORT, process.env.REDIS_HOST);
const hget = util.promisify(client.hget).bind(client);

module.exports = {

    async store(phone, unitId, bookTimestamp) {
        client.hmset(phone, unitId, 'unitId');
        client.expireat(unitId, bookTimestamp + 30 * 60 * 1000);
    },

    async get(phone, unitId) {
        return await hget(phone, unitId);
    },

};