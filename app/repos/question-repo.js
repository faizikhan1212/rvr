'use strict';

const redis = require('redis');
const util = require('util');
const client = redis.createClient(process.env.REDIS_PORT, process.env.REDIS_HOST);
const hgetall = util.promisify(client.hgetall).bind(client);
const hkeys = util.promisify(client.hkeys).bind(client);
const hget = util.promisify(client.hget).bind(client);

module.exports = {

    async store(company, questions) {
        const keys = await hkeys(company);

        if (keys.length > 0) {
            await client.hdel(company, keys);
        }
        questions.forEach((item, index) => client.hmset(company, index, JSON.stringify(item)));
    },

    async list(company) {
        return await hgetall(company);
    },

    async get(company, key) {
        return await hget(company, key);
    }

};