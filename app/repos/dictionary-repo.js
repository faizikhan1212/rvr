'use strict';

const redis = require('redis');
const util = require('util');
const client = redis.createClient(process.env.REDIS_PORT, process.env.REDIS_HOST);
const hget = util.promisify(client.hget).bind(client);

module.exports = {

    async store(company, dictionary) {
        client.hmset(`${company}-dictionary`, 'dictionary', JSON.stringify(dictionary));
    },

    async get(company) {
        return await hget(`${company}-dictionary`, 'dictionary');
    }

};