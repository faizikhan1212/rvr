'use strict';

const redis = require('redis');
const util = require('util');
const client = redis.createClient(process.env.REDIS_PORT, process.env.REDIS_HOST);
const hget = util.promisify(client.hget).bind(client);

module.exports = {

    async store(company, availability) {
        client.hmset(`${company}-availability`, 'availability', JSON.stringify(availability));
    },

    async get(company) {
        return await hget(`${company}-availability`, 'availability');
    }

};