'use strict';

const redis = require('redis');
const util = require('util');
const client = redis.createClient(process.env.REDIS_PORT, process.env.REDIS_HOST);
const hget = util.promisify(client.hget).bind(client);

module.exports = {

    async store(msg) {
        client.hmset(`${msg.conToken}-flow`, 'flow', JSON.stringify(msg));
    },

    async get(msg) {
        return await hget(`${msg.conToken}-flow`, 'flow');
    },

};