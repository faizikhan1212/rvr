'use strict';

const redis = require('redis');
const util = require('util');
const client = redis.createClient(process.env.REDIS_PORT, process.env.REDIS_HOST);
const hgetall = util.promisify(client.hgetall).bind(client);
const hkeys = util.promisify(client.hkeys).bind(client);
const recognizeAnswer = require('../helpers/recognize-answer');

module.exports = {

    async store(conToken, position, answer, questionType, answerType) {
        client.hmset(conToken, position, JSON.stringify({
            answer: await recognizeAnswer(answer, answerType),
            questionType: questionType,
            answerType: answerType,
        }));
    },

    async list(conToken) {
        return await hgetall(conToken);
    },

    async destroy(conToken) {
        const keys = await hkeys(conToken);

        if (keys.length > 0) {
            await client.hdel(conToken, keys);
        }
    },

};