'use strict';

const redis = require('redis');
const util = require('util');
const client = redis.createClient(process.env.REDIS_PORT, process.env.REDIS_HOST);
const hget = util.promisify(client.hget).bind(client);

module.exports = {

    async store(msg, username) {
        client.hmset(`${msg.conToken}-username`, 'username', username);
    },

    async get(msg) {
        return await hget(`${msg.conToken}-username`, 'username');
    },

    async storeByPhone(msg, username) {
        client.hmset(`${msg.phone}-username`, 'username', username);
    },

    async getByPhone(msg) {
        return await hget(`${msg.phone}-username`, 'username');
    },

};