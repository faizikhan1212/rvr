const util = require('util');
const redis = require('redis');
const nlp = require('compromise');
const yesNoService = require('../services/yes-no-service');
const leadCreation = require('../kafka/produces/lead-creation');
const questionRepo = require('../repos/question-repo');
const answerRepo = require('../repos/answer-repo');
const detectQuestionType = require('../helpers/detect-question-type');
const scheduleCreation = require('../kafka/produces/schedule-creation');
const auth = require('../auth/auth');
const bookableRepo = require('../repos/bookable-repo');
const usernameRepo = require('../repos/username-repo');
const isBookedUnit = require('../helpers/is-booked-unit');
const changePhoneService = require('../services/change-phone-service');
const bookServices = require('../services/book-a-tour-services');


const client = redis.createClient(process.env.REDIS_PORT, process.env.REDIS_HOST);
const hgetall = util.promisify(client.hgetall).bind(client);


async function bookATour(socket, event, msg) {

    let isBooked = false;

    if (msg.phone !== undefined && msg.phone !== null && msg.phone !== '') {
        isBooked = await isBookedUnit(socket, bookableRepo, msg);

        if (isBooked) {
            return false;
        }
    }

    if (msg.text === 'Calendar Cancel') {
        socket.emit('bot', {
            text: 'Changed your mind? No problem, you can get back to booking later on. 😉 Feel free to ask me any questions to make sure this apartment is what you\'re looking for.',
            phone: msg.phone,
            type: 'calendar-cancel',
            conToken: msg.conToken,
            company: msg.company,
        });

        return bookServices(socket, msg.phone, msg.conToken, msg.company);
    }

    switch (msg.type) {
        case 'user-name': {
            const data = nlp(msg.text).people().data();
            const fullNameInfo = data[0];

            if (fullNameInfo !== undefined) {
                await usernameRepo.store(msg, fullNameInfo.text.trim());
                return auth(socket, event, msg, 'phone-number');
            }

            if (msg.text.length < 255) {
                await usernameRepo.store(msg, msg.text.trim());
            }

            return auth(socket, event, msg, 'phone-number');
        }
        case 'phone-number': {
            return await auth(socket, event, msg, 'phone-number');
        }
        case 'confirm-code': {
            const confirmCode = await auth(socket, event, msg, 'confirm-code');

            if (confirmCode) {

                msg.type = 'ready-book-a-tour';
                const booked = await isBookedUnit(socket, bookableRepo, msg);

                if (booked) {
                    return false;
                }

                const $questions = questionRepo.list(msg.company);
                const $answers = answerRepo.list(msg.conToken);

                const [
                    questions,
                    answers,
                ] = [
                    await $questions,
                    await $answers,
                ];

                if (questions === null && answers === null || answers !== null && Object.keys(questions).length === Object.keys(answers).length) {
                    return leadCreation(msg, 'Lead Viewing', socket.id, answers);
                }

                const item = JSON.parse(await questionRepo.get(msg.company, 0));
                await answerRepo.destroy(msg.conToken);

                if (item !== null) {
                    socket.emit('bot', {
                        text: 'Great! A few quick questions here to prepare a note for the owner ...',
                        phone: msg.phone,
                        type: 'ready-book-a-tour',
                        conToken: msg.conToken,
                        company: msg.company,
                        position: 1,
                        context: msg.context,
                    });

                    socket.emit('bot', {
                        text: item.question,
                        questionType: item.type,
                        phone: msg.phone,
                        type: 'ready-book-a-tour',
                        conToken: msg.conToken,
                        company: msg.company,
                        position: 1,
                        context: msg.context,

                    });

                    if (item.type === 'Yes/No') {
                        return yesNoService(socket, msg.phone, msg.conToken, msg.company);
                    }
                    return false;
                }
            }

            socket.emit('bot', {
                text: 'Please enter the correct code.',
                phone: msg.phone,
                type: 'confirm-code',
                conToken: msg.conToken,
                company: msg.company,
                context: msg.context,
            });

            return changePhoneService(socket, msg.phone, msg.conToken, msg.company);
        }
        case 'ready-book-a-tour':
        case 'lead-disqualified':
        case 'calendar': {

            if (msg.phone !== undefined && msg.phone !== null && msg.phone !== '') {
                const obj = await hgetall(msg.phone);

                if (obj !== null) {

                    if (obj.confirm === 'true') {

                        if (isBooked) {
                            return false;
                        }

                        switch (msg.type) {
                            case 'ready-book-a-tour': {
                                const $questions = questionRepo.list(msg.company);
                                const $answers = answerRepo.list(msg.conToken);

                                const [
                                    questions,
                                    answers,
                                ] = [
                                    await $questions,
                                    await $answers,
                                ];

                                if (answers === null || questions !== null && Object.keys(questions).length !== Object.keys(answers).length) {

                                    if (msg.position !== undefined && msg.position !== null && msg.position !== '' && parseInt(msg.position) !== 0) {
                                        const position = parseInt(msg.position);
                                        const prevPosition = position - 1;
                                        const prev = JSON.parse(await questionRepo.get(msg.company, prevPosition));
                                        const item = JSON.parse(await questionRepo.get(msg.company, position));
                                        const questionType = detectQuestionType(prev.question);

                                        await answerRepo.store(msg.conToken, prevPosition, msg.text, questionType, prev.type);

                                        if (item !== null) {

                                            socket.emit('bot', {
                                                text: item.question,
                                                questionType: item.type,
                                                phone: msg.phone,
                                                type: 'ready-book-a-tour',
                                                conToken: msg.conToken,
                                                company: msg.company,
                                                position: position + 1,
                                                context: 'book-a-tour',
                                            });

                                            if (item.type === 'Yes/No') {
                                                return yesNoService(socket, msg.phone, msg.conToken, msg.company);
                                            }
                                            return false;
                                        }


                                        const lastAnswers = await answerRepo.list(msg.conToken);

                                        if (questions === null && lastAnswers === null || questions !== null && lastAnswers !== null && Object.keys(questions).length === Object.keys(lastAnswers).length) {
                                            return leadCreation(msg, 'Lead Viewing', socket.id, lastAnswers);
                                        }

                                        return false;
                                    }

                                    const item = JSON.parse(await questionRepo.get(msg.company, 0));
                                    await answerRepo.destroy(msg.conToken);

                                    if (item !== null) {

                                        socket.emit('bot', {
                                            text: item.question,
                                            questionType: item.type,
                                            phone: msg.phone,
                                            type: 'ready-book-a-tour',
                                            conToken: msg.conToken,
                                            company: msg.company,
                                            position: 1,
                                            context: 'book-a-tour',
                                        });

                                        if (item.type === 'Yes/No') {
                                            return yesNoService(socket, msg.phone, msg.conToken, msg.company);
                                        }
                                        return false;
                                    }
                                }
                                return leadCreation(msg, 'Lead Viewing', socket.id, answers);
                            }
                            case 'lead-disqualified': {
                                return socket.emit('bot', {
                                    text: `I'm sorry this unit doesn't meet your needs. 😕 But I've got some other amazing suggestions for you. <a href="/#/company/${msg.company}">Want to see them?</a> 🙂`,
                                    phone: msg.phone,
                                    type: 'lead-disqualified',
                                    conToken: msg.conToken,
                                    company: msg.company,
                                });
                            }
                            case 'calendar': {
                                const currentDate = Date.now();
                                const bookDate = Date.parse(msg.text);

                                if (bookDate > currentDate) {
                                    const offset = await scheduleCreation(msg);

                                    if (offset !== undefined && offset !== null) {
                                        await bookableRepo.store(msg.phone, msg.unitId, bookDate);

                                        socket.emit('bot', {
                                            text: 'Very nice! Running to the office to deliver your note and book your tour!',
                                            phone: msg.phone,
                                            type: 'finish-book-a-tour',
                                            conToken: msg.conToken,
                                            company: msg.company,
                                        });

                                        socket.emit('bot', {
                                            text: 'Good news! 🔔🔔 Your note went over very well! Can\'t wait to see you soon.',
                                            phone: msg.phone,
                                            type: 'finish-book-a-tour',
                                            conToken: msg.conToken,
                                            company: msg.company,
                                        });

                                        socket.emit('bot', {
                                            text: `I've got a few other amazing suggestions for you. 😉 <a href="/#/company/${msg.company}">Want to see them?</a> 🙂`,
                                            phone: msg.phone,
                                            type: 'finish-book-a-tour',
                                            conToken: msg.conToken,
                                            company: msg.company,
                                        });
                                    }
                                    return false;
                                }

                                socket.emit('bot', {
                                    text: 'The time of a showing must be greater than the current time 😉',
                                    phone: msg.phone,
                                    type: 'finish-book-a-tour',
                                    conToken: msg.conToken,
                                    company: msg.company,
                                });

                                return bookServices(socket, msg.phone, msg.conToken, msg.company);
                            }
                        }
                    }
                }

                socket.emit('bot', {
                    text: 'Please check your phone and enter your confirmation code.',
                    phone: msg.phone,
                    type: 'confirm-code',
                    conToken: msg.conToken,
                    company: msg.company,
                });

                return changePhoneService(socket, msg.phone, msg.conToken, msg.company);
            }

            const username = await usernameRepo.get(msg);

            if (username !== null) {
                return auth(socket, event, msg, 'phone-number');
            }

            return socket.emit('bot', {
                text: 'Can I have your full name please?',
                phone: msg.phone,
                type: 'user-name',
                conToken: msg.conToken,
                company: msg.company,
                context: msg.context,
            });
        }
    }

    return false;
}

module.exports = bookATour;