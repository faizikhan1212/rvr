const http = require('http');

const socketIO = require('socket.io');
const dot = require('dotenv');

dot.config();

const server = http.createServer();
const io = socketIO(server);
const train = require('./nlp/train');
const rand = require('./helpers/rand');
const services = require('./services/services');
const bookServices = require('./services/book-a-tour-services');
const findAvailableUnits = require('./find-available-units/find-available-units');
const bookATour = require('./book-a-tour/book-a-tour');
const askAQuestion = require('./ask-a-question/ask-a-question');
const flowRepo = require('./repos/flow-repo');

require('./kafka/consumer')(io);

io.on('connection', (socket) => {
    console.log('Socket client connected');

    socket.on('disconnect', () => {
        console.log('Socket client disconnected');
    });

    socket.on('bot', async (msg) => {

        if (msg.conToken !== undefined && msg.conToken !== null) {

            if (msg.text === '') {
                const flow = await flowRepo.get(msg);
                const flowMsg = JSON.parse(flow);

                if (flow !== null) {

                    if (msg.page === flowMsg.page) {
                        if (msg.unitId !== flowMsg.unitId) {
                            flowMsg.position = null;
                            flowMsg.text = '';
                            flowMsg.context = '';
                            flowMsg.unitId = msg.unitId;
                        }
                        msg = flowMsg;
                    }

                    if (msg.page !== flowMsg.page) {
                        msg.position = null;
                        msg.text = '';
                        msg.context = '';
                    }

                }

            }

            if (msg.text !== '') {
                await flowRepo.store(msg);
            }
        }


        if (msg.page === 'find-available-units') {

            if (msg.text === '' && (msg.conToken === undefined || msg.conToken === null || msg.conToken === '')) {
                const phone = msg.phone === null ? '' : msg.phone;
                const conToken = rand();

                socket.emit('bot', {
                    text: 'Hi! Hunting for a place is fun when you know someone who can help, right? 🙂 I\'m River, an AI-powered assistant built by former real estate agents here to make your search easier.',
                    phone: phone,
                    type: 'welcome',
                    conToken: conToken,
                    company: msg.company,
                });

                return services(socket, phone, conToken, msg.company);
            }

            return findAvailableUnits(socket, await train.process('en', msg.text), msg);
        }

        if (msg.page === 'book-a-tour') {

            if (msg.conToken === undefined || msg.conToken === null || msg.conToken === '') {
                const phone = msg.phone === null ? '' : msg.phone;
                msg.conToken = rand();

                socket.emit('bot', {
                    text: 'Hi! Really nice apartment, right? Cozy, and warm in the wintertime 🙂 I\'m River here to make your search easier. Ready to get on the books?',
                    phone: phone,
                    type: 'welcome',
                    conToken: msg.conToken,
                    company: msg.company,
                });

                return bookServices(socket, phone, msg.conToken, msg.company);
            }

            const event = await train.process('en', msg.text);

            switch (event.intent) {
                case 'book-a-tour': {
                    msg.type = 'ready-book-a-tour';
                    msg.context = 'book-a-tour';
                    break;
                }
                case 'ask-a-question': {
                    msg.context = 'ask-a-question';
                    break;
                }
            }

            switch (msg.context) {
                case 'book-a-tour': {
                    return await bookATour(socket, event, msg);
                }
                case 'ask-a-question': {
                    return await askAQuestion(io, socket, event, msg);
                }
            }

            return bookServices(socket, msg.phone, msg.conToken, msg.company);
        }
    });
});


server.listen(process.env.PORT);

