const redis = require('redis');
const dot = require('dotenv');
const Twilio = require('twilio');
const crypto = require('crypto');
const util = require('util');
const usernameRepo = require('../repos/username-repo');
const changePhoneService = require('../services/change-phone-service');

dot.config();

const twilio = new Twilio(process.env.TWILIO_ACCOUNT_SID, process.env.TWILIO_AUTH_TOKEN);
const client = redis.createClient(process.env.REDIS_PORT, process.env.REDIS_HOST);
const hget = util.promisify(client.hget).bind(client);

async function auth(socket, event, msg, switcher) {

    if (msg.phone === null) {
        return phoneNumber(socket, msg);
    }

    switch (switcher) {
        case 'phone-number': {
            const entity = event.entities.find((item) => item.entity === 'phonenumber');

            if (entity !== undefined && entity.sourceText.length > 0) {
                const code = generateCode();
                const entityPhone = twilioPhone(entity.sourceText);

                await sendCodeByPhone(entityPhone, code);

                socket.emit('bot', {
                    text: 'Please check your phone and enter your confirmation code.',
                    phone: entityPhone,
                    type: 'confirm-code',
                    conToken: msg.conToken,
                    company: msg.company,
                    context: msg.context,
                });

                return changePhoneService(socket, entityPhone, msg.conToken, msg.company);
            }

            return phoneNumber(socket, msg);
        }
        case 'confirm-code': {
            const entity = event.entities.find((item) => item.entity === 'number');
            const msgPhone = twilioPhone(msg.phone);

            if (entity !== undefined && entity.sourceText.length > 0) {
                const code = await hget(msgPhone, 'code');

                if (code === entity.sourceText) {
                    client.hset(msgPhone, 'confirm', true);
                    const username = await usernameRepo.get(msg);
                    await usernameRepo.storeByPhone(msg, username);
                    return true;
                }
            }

            return false;
        }
    }
}

async function sendCodeByPhone(to, code) {
    try {
        const from = process.env.TWILIO_FROM_NUMBER;
        const mode = process.env.TWILIO_MODE;

        if (mode === 'production') {

            const sid = await twilio.messages.create({
                from: from,
                to: to,
                body: `${code} - This is your confirmation code to book a tour. Please enter it to confirm your showing.`,
            });

            // console.log(sid);
            return sid ? client.hmset(to, 'code', code, 'confirm', false) : false;
        }

        return client.hmset(to, 'code', 1111, 'confirm', false);
    } catch (error) {
        console.log(error);
    }
}

function generateCode(cnt = 4) {
    const chars = '0123456789';
    const rnd = crypto.randomBytes(cnt);
    const value = new Array(cnt);
    const len = Math.min(256, chars.length);
    const d = 256 / len;

    for (let i = 0; i < cnt; i++) {
        value[i] = chars[Math.floor(rnd[i] / d)];
    }

    return value.join('');
}

function twilioPhone(to) {
    if (to[0] !== '+') {
        return `+1${to}`.replace(/[()\- ]+/gi, '');
    }
    return to;
}

function phoneNumber(socket, msg) {
    return socket.emit('bot', {
        text: 'Can you please provide your phone number as well? We will send you a code to confirm that you\'re not a bot 🙂',
        phone: '',
        type: 'phone-number',
        conToken: msg.conToken,
        company: msg.company,
        context: msg.context,
    });
}

module.exports = auth;