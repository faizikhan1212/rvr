# LetHub Chatbot

## Step-by-step
1. Install Packages
2. Run backend

#### Packages
*Install modules*       
`yarn install`

#### Docker & Redis
*Run containers with logging*   
`docker-compose up`

*Run containers without logging*     
`docker-compose up -d`

*Stop containers*   
`docker-compose down`

#### Packages
*Install Backend*    
`yarn install`

#### Environments
*Copy .env.example to .env*    
`cp .env.example .env`  
`cp .docker-compose.example .docker-compose`

#### Backend
*Run yarn with nodemon (dev)*   
`yarn start`

*Run yarn without nodemon (dev)*   
`yarn dev`

*Run yarn (prod)*   
`yarn prod`