define start_task =
    yarn start
endef

define build_task =
    npm install yarn
    yarn install
endef

define build_start_task =
    npm install yarn
    yarn install
    yarn start
endef

run: ; $(value $(TASK))
.ONESHELL: